const express = require('express');
const contentController = require('../controllers/contentController')

const router = express.Router();


router.get('/api', contentController.homePage)

module.exports = router;